package com.example.courier_system.controller;

import com.example.courier_system.dto.OrderDTO;
import com.example.courier_system.service.PlaceOrderService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.annotation.KafkaListeners;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequiredArgsConstructor
@CrossOrigin
@RequestMapping("api/v1/place-order")
public class PlaceOrderController {

    private final PlaceOrderService placeOrderService;

    @GetMapping("/{userId}")
    public ResponseEntity<?> getOrder(@PathVariable Long userId) {
        return ResponseEntity.ok(placeOrderService.getOrders(userId));
    }

    @PostMapping("/save-order")
    public ResponseEntity<?> placeOrder(@RequestBody OrderDTO orderDTO) {
        try {
            boolean placeOrder = placeOrderService.placeOrder(orderDTO);
            if (placeOrder){
                return ResponseEntity.ok("Order Placed Successfully");
            }else {
                return ResponseEntity.status(500).body("Server Error");
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ResponseEntity.badRequest().body("Something wrong!!!");
        }
    }

    @PostMapping("/update-order")
    public ResponseEntity<?> updateOrder(OrderDTO orderDTO) {
        try {
            boolean placeOrder = placeOrderService.updatePlaceOrder(orderDTO);
            if (placeOrder){
                return ResponseEntity.ok("Order Update Successfully");
            }else {
                return ResponseEntity.status(500).body("Server Error");
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @DeleteMapping("/delete-order/{orderId}")
    public ResponseEntity<?> deleteOrder(@PathVariable Long orderId) {
        try {
            return ResponseEntity.ok(placeOrderService.deletePlaceOrder(orderId));
        } catch (Exception e) {
            log.error(e.getMessage(),e);
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }
}
