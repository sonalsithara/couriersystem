package com.example.courier_system.service.impl;

import com.example.courier_system.dto.OrderDTO;
import com.example.courier_system.entity.Courier;
import com.example.courier_system.entity.Order;
import com.example.courier_system.repository.CourierRepository;
import com.example.courier_system.repository.OrderRepository;
import com.example.courier_system.service.PlaceOrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Service
@RequiredArgsConstructor
public class PlaceOrderServiceImpl implements PlaceOrderService {

    private final static String EMPTY = "";
    private final static String EMAIL_MESSAGE = "<b>Hey There</b>,<br><i>Here are the details for the delivery</i>" +
            "<h1>Sender Name:- %s</h1><br>" +
            "<h1>Sender Address:- %s</h1><br>" +
            "<h1>Receiver Name:- %s</h1><br>" +
            "<h1>Receiver Address:- %s</h1><br>";
    private final static String EMAIL_SUBJECT = "Some one place an order";

    private final JavaMailSender mailSender;
    private final OrderRepository orderRepository;
    private final CourierRepository courierRepository;
    private final KafkaTemplate<String, String> kafkaTemplate;

    @Override
    public boolean placeOrder(OrderDTO orderDTO) throws Exception {

        isDtoNull(orderDTO);

        Order order = new Order();
        BeanUtils.copyProperties(orderDTO, order);
        Order saveOrder = orderRepository.save(order);

        Courier courier = courierRepository.findFirstByCode(saveOrder.getCourierCompanyCode());
        sendEmil(saveOrder.getSenderName(), saveOrder.getSenderAddress(), saveOrder.getReceiverName(),
                saveOrder.getReceiverAddress(), courier.getEmail(), EMAIL_MESSAGE, EMAIL_SUBJECT);

        return true;
    }

    @Override
    public boolean updatePlaceOrder(OrderDTO orderDTO) throws Exception {

        isDtoNull(orderDTO);

        boolean existsById = orderRepository.existsById(orderDTO.getId());
        if (existsById) {
            Order order = new Order();
            BeanUtils.copyProperties(orderDTO, order);
            Order updateOrder = orderRepository.save(order);
        } else {
            return false;
        }
        return true;
    }

    @Override
    public boolean deletePlaceOrder(Long orderId) throws Exception {
        boolean value = isNull(orderId);
        if (!value) {
            orderRepository.deleteById(orderId);
        } else {
            return false;
        }
        return false;
    }

    @Override
    public OrderDTO getOrders(Long userId) {
        return orderRepository.getOrderByUserId(userId);
    }

    private void isDtoNull(OrderDTO orderDTO) throws Exception {
        if (isNullOrEmpty(orderDTO.getSenderAddress()) || isNullOrEmpty(orderDTO.getReceiverAddress()) ||
                isNullOrEmpty(orderDTO.getSenderName()) || isNullOrEmpty(orderDTO.getReceiverName()) ||
                isNullOrEmpty(orderDTO.getCourierCompanyCode().toString()) || isNullOrEmpty(orderDTO.getParcelWeight()) ||
                isNullOrEmpty(orderDTO.getUserId().toString())) {
            throw new RuntimeException("Fields Can't be Empty");
        }
    }

    private boolean isNullOrEmpty(String value) {
        return null == value || EMPTY.equals(value);
    }

    private boolean isNull(Long value) throws Exception {
        return null == value;
    }

    private void sendEmil(String senderName, String senderAddress, String recevierName, String recevierAddress,
                          String toEmail, String body, String subject) throws MessagingException {

        String from = "sampathdewage123@gmail.com";

        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);

        helper.setFrom(from);
        helper.setTo(toEmail);
        helper.setSubject(subject);

        boolean html = true;
        helper.setText(String.format(body, senderName, senderAddress, recevierName, recevierAddress), html);

        mailSender.send(message);
    }

}
